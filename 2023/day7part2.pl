use strict;
use warnings;
use Data::Dumper;

sub println {
	print @_;
	print "\n";
}

sub cards_letters_to_numbers {
	return 10 if $_ eq "T";
	return 1 if $_ eq "J";
	return 12 if $_ eq "Q";
	return 13 if $_ eq "K";
	return 14 if $_ eq "A";
	return int $_;
}

sub two_level_nested_int_array {
	for (my $i = 0; $i < @{$a}; $i++) {
		my $result = @{$a}[$i] <=> @{$b}[$i];
		return $result if $result != 0;
	}

	return 0;
}

sub evaluate_hand {
	my ($c) = @_;
	my @cards = @{$c};
	print Dumper \@cards;
	my @hand = map { cards_letters_to_numbers($_) } @cards;
	my @result;
	println "Hand strength: ", hand_strength (\@hand);
	push @result, hand_strength (\@hand);
	push @result, @hand;
	return @result;
}

sub frequencies {
	my ($h) = @_;
	my @hand = @{$h};

	my %hand;
	foreach (@hand) {
		$hand{$_} = 0 if not defined $hand{$_};
		$hand{$_} += 1;
	}

	my @sorted_frequencies = sort { $b <=> $a } values %hand;
	return @sorted_frequencies;
}

sub hand_strength {
	my ($h) = @_;
	my @hand = @{$h};

	my @hand_without_joker = grep { $_ != 1 } @hand;
	my @sorted_frequencies_without_joker = frequencies(\@hand_without_joker);

	my $joker_count = scalar grep { $_ == 1 } @hand;
	$sorted_frequencies_without_joker[0] += $joker_count;

	println Dumper \@sorted_frequencies_without_joker;

	return 7 if $sorted_frequencies_without_joker[0] == 5;
	return 6 if $sorted_frequencies_without_joker[0] == 4;
	return 5 if $sorted_frequencies_without_joker[0] == 3 and $sorted_frequencies_without_joker[1] == 2;
	return 4 if $sorted_frequencies_without_joker[0] == 3;
	return 3 if $sorted_frequencies_without_joker[0] == 2 and $sorted_frequencies_without_joker[1] == 2;
	return 2 if $sorted_frequencies_without_joker[0] == 2;
	return 1 if $sorted_frequencies_without_joker[0] == 1;
	return -1;
}

my @all_hands;

while (my $line = <>) {
	chomp $line;
	my ($cards, $bid) = split / /, $line;
	my @cards = split //, $cards;
	my @hand_as_int = evaluate_hand (\@cards);
	$bid = int $bid;
	push @hand_as_int, $bid;
	push @all_hands, [ @hand_as_int ];
}

print Dumper \@all_hands;

my @all_hands_sorted = sort two_level_nested_int_array @all_hands;

print Dumper \@all_hands_sorted;

my $result;
for (my $i = 0; $i < @all_hands_sorted; $i++) {
	$result += ($i+1) * $all_hands_sorted[$i][6];
}

println "Answer: $result";

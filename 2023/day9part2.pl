use strict;
use warnings;

sub println {
	print(@_);
	print("\n");
}

sub find_next {
	my ($s) = @_;
	my @s = @{$s};
	if (scalar grep { $_ != 0 } @s > 0) {
		my @diff;
		for (my $i = 1; $i < @s; $i++) {
			push @diff, $s[$i]-$s[$i-1];
		}
		my $last = scalar @s - 1;
		return $s[$last] + find_next(\@diff);
	}

	# base case
	return 0;
}

my $sum;
while (my $line = <>) {
	chomp $line;
	my @seq = reverse split / /, $line;
	$sum += find_next(\@seq);
}

println $sum;

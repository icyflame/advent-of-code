use strict;
use warnings;

use Data::Dumper;
use Math::BigInt;

my $VAR1 = [
          13301,
          14999,
          18961,
          16697,
          12169,
          17263
        ];

my $a1 = Math::BigInt->new(13301);
my $a2 = Math::BigInt->new(14999);
my $a3 = Math::BigInt->new(18961);
my $a4 = Math::BigInt->new(16697);
my $a5 = Math::BigInt->new(12169);
my $a6 = Math::BigInt->new(17263);
sub println {
	print @_;
	print "\n";
}

println $a1->blcm($a2, $a3, $a4, $a5, $a6);

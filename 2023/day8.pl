use strict;
use warnings;

use Data::Dumper;

sub println {
	print @_;
	print "\n";
}

my $order = '';

my %network;

while (my $line = <>) {
	chomp $line;

	if ($order eq '') {
		$order = $line;
		next;
	}

	next if $line eq '';

	$line =~ m#^([A-Z]{3}) = .([A-Z]{3}), ([A-Z]{3}).$#;
	my $source = $1;
	$network{$source}{'L'} = $2;
	$network{$source}{'R'} = $3;
}

print Dumper \%network;

my $start = "AAA";
my $end = "ZZZ";

sub find_step_count {
	my ($start, $end, $network, $order) = @_;
	my %network = %{$network};

	my $current = $start;
	my @order = split //, $order;
	my $i = 0;
	my $step_count = 0;
	do {
		if ($step_count > 100000) {
			println "infinite loop detection";
			exit 43;
		}

		my $next = $network{$current}{$order[$i]};
		println "$current => $next ($end)";

		$current = $next;
		$i = ($i + 1) % @order;
		$step_count++;
	} while ($current ne $end);
	return $step_count;
}

my $answer = find_step_count ($start, $end, \%network, $order);
println "Answer: $answer";

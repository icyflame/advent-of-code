use strict;
use warnings;

use Data::Dumper;

sub println {
	print(@_);
	print("\n");
}

use POSIX qw(ceil);
sub rand_int {
	return ceil (rand() * 10000) % 10;
}

my @a = (
	[ rand_int(), rand_int(), rand_int(), ],
	[ rand_int(), rand_int(), rand_int(), ],
	[ 0, 4, rand_int(), ],
	[ 0, 4, rand_int(), ],
	[ rand_int(), rand_int(), rand_int(), ],
	);

my @sorted = sort two_level_nested_int_array @a;

print Dumper \@a;
print Dumper \@sorted;

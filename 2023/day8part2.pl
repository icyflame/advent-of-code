use strict;
use warnings;

use Data::Dumper;

sub println {
	print @_;
	print "\n";
}

my $order = '';

my %network;

while (my $line = <>) {
	chomp $line;

	if ($order eq '') {
		$order = $line;
		next;
	}

	next if $line eq '';

	$line =~ m#^([0-9A-Z]{3}) = .([0-9A-Z]{3}), ([0-9A-Z]{3}).$#;
	my $source = $1;
	$network{$source}{'L'} = $2;
	$network{$source}{'R'} = $3;
}

print Dumper \%network;

# my @current = @start;

# my @order = split //, $order;
# # print Dumper \@order;

# my $i = 0;
# my $step_count = 0;
# do {
# 	if ($step_count > 10) {
# 		println "infinite loop detection";
# 		exit 43;
# 	}

# 	my @next;
# 	foreach (@current) {
# 		push @next, $network{$_}{$order[$i]};
# 	}

# 	println $step_count, ": ", join(", ", @current), " => ", join(", ", @next);

# 	@current = @next;

# 	$i = ($i + 1) % @order;
# 	$step_count++;
# } while (scalar (grep { $_ =~ m#Z$# } @current) != scalar (@current));

# println "Answer: $step_count";

sub find_step_count {
	my ($start, $end, $network, $order) = @_;
	my %network = %{$network};

	my $current = $start;
	my @order = split //, $order;
	my $i = 0;
	my $step_count = 0;
	do {
		if ($step_count > 20000) {
			println "infinite loop detection";
			return -1;
		}

		my $next = $network{$current}{$order[$i]};
		println "$current => $next ($end)";

		$current = $next;
		$i = ($i + 1) % @order;
		$step_count++;
	} while ($current ne $end);
	return $step_count;
}

my @start = grep { $_ =~ m#A$# } keys %network;
my @end = grep { $_ =~ m#Z$# } keys %network;

my @counts;
foreach my $s (@start) {
	foreach my $e (@end) {
		my $step_count = find_step_count ($s, $e, \%network, $order);
		println "Result: $s => $e: $step_count";
		push @counts, $step_count if $step_count != -1;
	}
}

println Dumper \@counts;
# my $answer = 1;
# foreach (@counts) { $answer *= $_; }
# println "Answer: $answer";

use Math::BigInt;
my $answer = Math::BigInt->bone();
foreach (@counts) {
	my $c = Math::BigInt->new($_);
	$answer->blcm($c);
}
println $answer;

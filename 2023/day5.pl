use strict;
use warnings;

use Data::Dumper;

sub println {
	print @_;
	print "\n";
}

sub print_seeds {
	my ($o) = @_;
	my @seeds = @{$o};
	for (my $i = 0; $i < @seeds; $i++) {
		my $s = $seeds[$i][0];
		my $s_r = $seeds[$i][1];
		println "$s ($s_r) ", $s+$s_r;
	}
}

sub map_this {
	my ($o, $c) = @_;
	my @original = @{$o};
	my @current_map = @{$c};
	my $seed_count = 0;
	my $map_count = 0;
	my $current_seed_mapped = -1;

	my @after_mapping;

	do {
		my ($original_seed, $original_seed_r) = @{$original[$seed_count]};

		my $seed = $original_seed;
		my $seed_r = $original_seed_r;

		# A part of this seed might already have been mapped
		$seed = $original_seed + $current_seed_mapped if $current_seed_mapped != -1;
		$seed_r = $original_seed_r - $current_seed_mapped if $current_seed_mapped != -1;

		my ($map_d, $map_b, $map_r) = @{$current_map[$map_count]};

		println "Map $map_count: $map_b ($map_r) ", $map_b+$map_r, " (", $map_d-$map_b, ") | Seed $seed_count: $seed ($seed_r) ", $seed+$seed_r, " | Mapped: $current_seed_mapped";

		if ($map_b >= $seed+$seed_r) {
			println "1 - Map after seed";
			push @after_mapping, [ $seed, $seed_r ];
			$seed_count++;
			$current_seed_mapped = -1;
		}

		if ($map_b+$map_r < $seed) {
			println "2 - Seed after map";
			$map_count++;
		}

		if ($map_b <= $seed && $map_b+$map_r >= $seed+$seed_r) {
			println "3 - Seed contained in map";
			my $new_seed = $seed + $map_d - $map_b;
			push @after_mapping, [ $new_seed, $seed_r ];
			$seed_count++;
			$current_seed_mapped = -1;
		}

		if ($map_b < $seed && ($map_b+$map_r >= $seed && $map_b+$map_r < $seed+$seed_r)) {
			println "4 - Map overlap seed on the left";
			$map_count++;
			my $new_seed = $seed + $map_d - $map_b;
			my $new_r = $map_b + $map_r - $seed;
			push @after_mapping, [ $new_seed, $new_r ];
			$current_seed_mapped += $new_r if $current_seed_mapped != -1;
			$current_seed_mapped = $new_r if $current_seed_mapped == -1;
		}

		if ($map_b > $seed && ($seed+$seed_r >= $map_b && $map_b+$map_r > $seed+$seed_r)) {
			println "5 - Map overlap seed on the right";
			# Map the left part first to itself
			push @after_mapping, [ $seed, $map_b - $seed ];
			# Map the right part using the mapping
			push @after_mapping, [ $map_d, $seed + $seed_r - $map_b ];

			$seed_count++;
			$current_seed_mapped = -1;
		}

		if ($map_b >= $seed && $map_b+$map_r <= $seed+$seed_r) {
			println "6 - Map contained in seed";
			if ($seed != $map_b) {
				# Map the left part first to itself
				push @after_mapping, [ $seed, $map_b - $seed ];
			}
			# Map the right part
			push @after_mapping, [ $map_d, $map_r ];
			$current_seed_mapped = $map_b + $map_r - $original_seed;
			$map_count++;
		}

		print_seeds \@after_mapping;
	} while ($map_count < @current_map && $seed_count < @original);

	for (; $seed_count < @original; $seed_count++) {
		my ($original_seed, $original_seed_r) = @{$original[$seed_count]};

		my $seed = $original_seed;
		my $seed_r = $original_seed_r;

		# A part of this seed might already have been mapped
		$seed = $original_seed + $current_seed_mapped if $current_seed_mapped != -1;
		$seed_r = $original_seed_r - $current_seed_mapped if $current_seed_mapped != -1;

		println "No map | Seed $seed_count: $seed ($seed_r) ", $seed+$seed_r, " | Mapped: $current_seed_mapped";

		# Only a part of the first seed might have been mapped. Everything else is not mapped and
		# must be copied as-is by definition.
		$current_seed_mapped = -1;

		push @after_mapping, [ $seed, $seed_r ];
		print_seeds \@after_mapping;
	}

	@after_mapping = sort { @{$a}[0] <=> @{$b}[0] } @after_mapping;

	return @after_mapping;
}

# sub sum {
# 	my ($a) = @_;
# 	my @a = @{$a};
# 	my $sum;
# 	foreach (@a} {
# 		$sum += $_;
# 	}
# 	return $sum;
# }

my @lines;
my @original;

sub handle_lines {
	my ($l, $o) = @_;
	my @lines = @{$l};
	my @original = @{$o};
	my @current_map;
	for (my $i = 1; $i < @lines; $i++) {
		my ($d, $s, $r) = map { int $_ } split / /, $lines[$i];
		push @current_map, [ $d, $s, $r ];
	}
	@current_map = sort { @{$a}[1] <=> @{$b}[1] } @current_map;

	@original = map_this(\@original, \@current_map);
	println "-----";
	print_seeds \@original;

	my $sum;
	foreach (@original) {
		$sum += @{$_}[1];
	}
	println "seed count: $sum";
	@lines = ();

	return @original;
}

while (my $line = <>) {
	chomp $line;
	if (! $line eq '') {
		push @lines, $line;
		next;
	}

	if (scalar @lines == 1) {
		my $seed_line = $lines[0];
		$seed_line =~ s/seeds: //g;
		my @seeds = map { int $_ } split / /, $seed_line;
		for(my $i = 0; $i < @seeds/2; $i++) {
			push @original, [ $seeds[2*$i], $seeds[2*$i+1] ];
		}
		@original = sort { @{$a}[0] <=> @{$b}[0] } @original;
		print Dumper \@original;
		print_seeds \@original;
		@lines = ();
		next;
	}

	print Dumper \@lines;
	@original = handle_lines(\@lines, \@original);
	@lines = ();
}

if (@lines > 0) {
	print Dumper \@lines;
	@original = handle_lines(\@lines, \@original);
}

println "Answer: $original[0][0]";
